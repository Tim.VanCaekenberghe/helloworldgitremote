package string.generators;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({RandomUtils.class})
public class GeneratorMockTest {

    @Test
    public void testRandomFirstName() {
        //Mock instellen, resultaten van uw klasse die niet onder test
        PowerMockito.mockStatic(RandomUtils.class);
        when(RandomUtils.getRandomFirstName()).thenReturn("Yoni");
        when(RandomUtils.getRandomLastName()).thenReturn("Vindelinckx");

        String name = Generator.randomName();
        System.out.println("Name " + name);
        String[] splitted = name.split(" ");
        String first = splitted[0];
        String last = name.substring(first.length() + 1);
        assertEquals("Yoni", first);
        assertEquals("Vindelinckx", last);

        verifyStatic(RandomUtils.class, Mockito.times(1));
        RandomUtils.getRandomFirstName();
        verifyStatic(RandomUtils.class, Mockito.times(1));
        RandomUtils.getRandomLastName();
    }
}
